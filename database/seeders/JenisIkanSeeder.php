<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class JenisIkanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        
        $kode = ['1','2'];
        $descr = ['AIR TAWAR','AIR LAUT'];

        for ($i=0; $i <count($kode) ; $i++) { 
            DB::table('jenis_item')->insert([
                'kode_jenis' => $kode[$i],
                'description' => $descr[$i],
                'created_at' => Carbon::now(),
                
            ]);
        }
    }
}
