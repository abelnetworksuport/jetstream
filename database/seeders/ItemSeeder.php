<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kode = ['001','002'];
        $descr = ['Cakalang','Tuna'];
        $jenis = ['1','1'];

        for ($i=0; $i <count($kode) ; $i++) { 
            DB::table('master_item')->insert([
                'kode_item' => $kode[$i],
                'description' => $descr[$i],
                'jenis_id'  => $jenis[$i],
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
