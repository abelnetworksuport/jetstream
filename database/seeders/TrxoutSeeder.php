<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;
class TrxoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $inoutId = ['1','2'];
        $palletId = ['1','2'];
        $itemId = ['1','2'];
        $namaikan = ['Bandeng','LELE'];
        $jenisikan = ['jenis1','jenis2'];
        $berrat = ['10','20'];
        $rakId = ['1','2'];

        for ($i=0; $i <count($inoutId) ; $i++) { 
            # code...
            DB::table('trxstockout')->insert([
                // 'barcode' => $barcode[$i],
                'inout_id' => $inoutId[$i],
                'pallet_id' => $palletId[$i],
                'item_id' => $itemId[$i],
                'nama_ikan' => $namaikan[$i],
                'jenis_ikan' => $jenisikan[$i],
                'berat' => $berrat[$i],
                'rack_id' => $rakId[$i],
                'created_at' => Carbon::now(),
                
            ]);
        }
    }
}
