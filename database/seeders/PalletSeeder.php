<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class PalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kode = ['P-001-100','P-001-150'];
        $desc = ['Pallet 100kg','Pallet 150kg'];
        $weight = ['100','150'];
    
        for ($i=0; $i <count($kode) ; $i++) { 
            DB::table('master_pallet')->insert([
                'kode_pallet' => $kode[$i],
                'description' => $desc[$i],
                'weight'    => $weight[$i],
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
