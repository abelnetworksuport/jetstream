<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class RackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    
        $kode = ['A-001','A-002'];
        $desc = ['RACK A ROW 1','RACK A ROW 2'];

        for ($i=0; $i <count($kode) ; $i++) { 
            DB::table('master_rack')->insert([
                'kode_rack' => $kode[$i],
                'description' => $desc[$i],
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
