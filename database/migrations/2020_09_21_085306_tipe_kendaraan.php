<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipeKendaraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tipe_kendaraan', function (Blueprint $table) {
            $table->id();
            $table->string('kode_tipe')->unique();
            $table->string('type_kendaran')->nullable();
            $table->string('allias')->nullable();
            $table->string('jumlah_ban')->nullable();
            $table->string('volume_standart')->nullable();
            $table->string('kapasitas')->nullable();
            $table->string('dc_dry')->nullable();
            $table->string('dc_fresh')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tipe_kendaraan');
    }
}
