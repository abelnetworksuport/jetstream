<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterKendaraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master_kendaraan', function (Blueprint $table) {
            $table->id();
            $table->string('no_polisi');
            $table->string('tahun')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('perusahaan_id')->nullable();
            $table->integer('personal_id')->nullable();
            $table->string('status')->nullable();
            $table->string('country')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('status_get')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master_kendaraan');
    }
}
