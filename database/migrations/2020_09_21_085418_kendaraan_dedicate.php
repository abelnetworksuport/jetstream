<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KendaraanDedicate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('kendaraan_dedicated', function (Blueprint $table) {
            $table->id();
            $table->string('tujuan');
            $table->string('transporter');
            $table->string('customer');
            $table->string('pemesanan');
            $table->string('tyoe_kendaraan');
            $table->string('suhu');
            $table->string('dari_lokasi');
            $table->string('ke_lokasi');
            $table->string('keterangan');
            $table->string('jumlah_armada');
            $table->integer('qty_barang');
            $table->integer('uom_barang');
            $table->string('cc_mail');
            $table->timestamps();
            $table->softDeletes();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('kendaraan_dedicated');
    }
}
