<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterGate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master_gate', function (Blueprint $table) {
            $table->id();
            $table->string('kode_gate')->unique();
            $table->string('kelompok')->nullable();
            $table->string('nama')->nullable();
            $table->string('ante_room')->nullable();
            $table->string('printer')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master_gate');
    }
}
