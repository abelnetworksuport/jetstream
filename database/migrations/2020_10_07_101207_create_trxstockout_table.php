<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxstockoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trxstockout', function (Blueprint $table) {
            $table->id();
            $table->integer('rack_id')->nullable();
            $table->integer('pallet_id')->nullable();
            $table->integer('gate_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->string('nama_ikan')->nullable();
            $table->string('jenis_ikan')->nullable();
            $table->string('berat')->nullable();
            $table->integer('inout_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trxstockout');
    }
}
