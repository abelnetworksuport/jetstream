<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionInbound extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_inbound', function (Blueprint $table) {
            $table->id();
            $table->string('kode_trx')->nullable();
            $table->integer('tujuan')->nullable();
            $table->integer('kendaraan_id')->nullable();
            $table->string('pemesanan')->nullable();
            $table->string('gate')->nullable();
            $table->string('suhu')->nullable();
            $table->string('dari')->nullable();
            $table->string('ke')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_inbound');
    }
}
