<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterSupir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master_supir', function (Blueprint $table) {
            $table->id();
            $table->string('kode_personal')->unique();
            $table->string('no_id')->nullable();
            $table->string('nama')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kota')->nullable();
            $table->string('hp')->nullable();
            $table->string('kelompok')->nullable();
            $table->integer('perusahaan_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master_supir');
    }
}
