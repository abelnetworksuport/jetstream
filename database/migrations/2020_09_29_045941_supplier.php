<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Supplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('supplier', function (Blueprint $table) {
            $table->id();
            $table->string('kode_sup')->unique();
            $table->string('nama')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kota')->nullable();
            $table->string('hp')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('cp')->nullable();
            $table->integer('kelompok')->nullable();
            $table->integer('deposit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('supplier');
    }
}
