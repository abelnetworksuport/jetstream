<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
    
});

Route::middleware('auth:api')->group(function () {
    Route::get('/details', 'App\Http\Controllers\api\UserController@details');
    Route::get('/cari-getin', 'App\Http\Controllers\api\GetinController@cari_getin');
    Route::post('/post-getin', 'App\Http\Controllers\api\GetinController@post_getin');
    Route::get('/tampil-getin', 'App\Http\Controllers\api\GetinController@tampil_getin');

    Route::get('/cari-getout', 'App\Http\Controllers\api\GetoutController@cari_getout');
    Route::post('/post-getout', 'App\Http\Controllers\api\GetoutController@post_getout');
    Route::get('/tampil-getout', 'App\Http\Controllers\api\GetoutController@tampil_getout');

    Route::get('/cari-inbound', 'App\Http\Controllers\api\StockinController@cari_inbound');
    Route::get('/cari-item', 'App\Http\Controllers\api\StockinController@cari_item');
    Route::get('/cari-rack', 'App\Http\Controllers\api\StockinController@cari_rack');
    Route::get('/cari-pallet', 'App\Http\Controllers\api\StockinController@cari_pallet');
    Route::get('/cari-gate', 'App\Http\Controllers\api\StockinController@cari_gate');


    Route::post('/post-trxin', 'App\Http\Controllers\api\StockinController@post_trxin');
    Route::post('/post-trxout', 'App\Http\Controllers\api\StockinController@post_trxout');
});

Route::post('/login', 'App\Http\Controllers\api\UserController@login');


