<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});
Route::get('/test',function(){
    $user = Auth::user();

    return response($user);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard.dashboard');
})->name('dashboard');

/* Start::Master Gate  */

Route::get('/master/uom/index', function () {
    return view('master.uom');
});
Route::get('/master/uom/list', 'App\Http\Controllers\Master\UomController@uom_list')->name('uom_list');
Route::get('/master/uom/json', 'App\Http\Controllers\Master\UomController@uom_json')->name('uom_json');
Route::post('/master/uom/input', 'App\Http\Controllers\Master\UomController@uom_input')->name('uom_input');
Route::get('/master/uom/edit', 'App\Http\Controllers\Master\UomController@uom_edit')->name('uom_edit');
Route::post('/master/uom/update', 'App\Http\Controllers\Master\UomController@uom_update')->name('uom_update');
Route::post('/master/uom/delete', 'App\Http\Controllers\Master\UomController@uom_delete')->name('uom_delete');

Route::get('/master/gate/index', function () {
    return view('master.gate');
});
Route::get('/master/gate/list', 'App\Http\Controllers\Master\GateController@gate_list')->name('gate_list');
Route::get('/master/gate/json', 'App\Http\Controllers\Master\GateController@gate_json')->name('gate_json');
Route::post('/master/gate/input', 'App\Http\Controllers\Master\GateController@gate_input')->name('gate_input');
Route::get('/master/gate/edit', 'App\Http\Controllers\Master\GateController@gate_edit')->name('gate_edit');
Route::post('/master/gate/update', 'App\Http\Controllers\Master\GateController@gate_update')->name('gate_update');
Route::post('/master/gate/delete', 'App\Http\Controllers\Master\GateController@gate_delete')->name('gate_delete');

Route::get('/master/kota/index', function () {
    return view('master.kota');
});
Route::get('/master/kota/list', 'App\Http\Controllers\Master\KotaController@kota_list')->name('kota_list');
Route::get('/master/kota/json', 'App\Http\Controllers\Master\KotaController@kota_json')->name('kota_json');
Route::post('/master/kota/input', 'App\Http\Controllers\Master\KotaController@kota_input')->name('kota_input');
Route::get('/master/kota/edit', 'App\Http\Controllers\Master\KotaController@kota_edit')->name('kota_edit');
Route::post('/master/kota/update', 'App\Http\Controllers\Master\KotaController@kota_update')->name('kota_update');
Route::post('/master/kota/delete', 'App\Http\Controllers\Master\KotaController@kota_delete')->name('kota_delete');

Route::get('/master/supplier/index', function () {
    return view('master.supplier');
});
Route::get('/master/supplier/list', 'App\Http\Controllers\Master\SupplierController@supplier_list')->name('supplier_list');
Route::get('/master/supplier/json', 'App\Http\Controllers\Master\SupplierController@supplier_json')->name('supplier_json');
Route::post('/master/supplier/input', 'App\Http\Controllers\Master\SupplierController@supplier_input')->name('supplier_input');
Route::get('/master/supplier/edit', 'App\Http\Controllers\Master\SupplierController@supplier_edit')->name('supplier_edit');
Route::post('/master/supplier/update', 'App\Http\Controllers\Master\SupplierController@supplier_update')->name('supplier_update');
Route::post('/master/supplier/delete', 'App\Http\Controllers\Master\SupplierController@supplier_delete')->name('supplier_delete');

Route::get('/master/personal/index', function () {
    return view('master.personal');
});
Route::get('/master/personal/list', 'App\Http\Controllers\Master\SupirController@supir_list')->name('supir_list');
Route::get('/master/personal/json', 'App\Http\Controllers\Master\SupirController@supir_json')->name('supir_json');
Route::post('/master/personal/input', 'App\Http\Controllers\Master\SupirController@supir_input')->name('supir_input');
Route::get('/master/personal/edit', 'App\Http\Controllers\Master\SupirController@supir_edit')->name('supir_edit');
Route::post('/master/personal/update', 'App\Http\Controllers\Master\SupirController@supir_update')->name('supir_update');
Route::post('/master/personal/delete', 'App\Http\Controllers\Master\SupirController@supir_delete')->name('supir_delete');
Route::get('/master/personal/print/{id}', 'App\Http\Controllers\Master\SupirController@supir_print')->name('supir_print');
Route::get('/master/personal/print', 'App\Http\Controllers\Master\SupirController@supir_qr')->name('supir_qr');

Route::get('/master/tipe_kendaraan/index', function () {
    return view('master.tipe');
});
Route::get('/master/tipe_kendaraan/list', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_list')->name('tipe_list');
Route::get('/master/tipe_kendaraan/json', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_json')->name('tipe_json');
Route::post('/master/tipe_kendaraan/input', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_input')->name('tipe_input');
Route::get('/master/tipe_kendaraan/edit', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_edit')->name('tipe_edit');
Route::post('/master/tipe_kendaraan/update', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_update')->name('tipe_update');
Route::post('/master/tipe_kendaraan/delete', 'App\Http\Controllers\Master\TipeKendaraanController@tipe_delete')->name('tipe_delete');

Route::get('/master/kendaraan/index', function () {
    return view('master.kendaraan');
});
Route::get('/master/kendaraan/list', 'App\Http\Controllers\Master\KendaraanController@kendaraan_list')->name('kendaraan_list');
Route::get('/master/kendaraan/json', 'App\Http\Controllers\Master\KendaraanController@kendaraan_json')->name('kendaraan_json');
Route::post('/master/kendaraan/input', 'App\Http\Controllers\Master\KendaraanController@kendaraan_input')->name('kendaraan_input');
Route::get('/master/kendaraan/edit', 'App\Http\Controllers\Master\KendaraanController@kendaraan_edit')->name('kendaraan_edit');
Route::post('/master/kendaraan/update', 'App\Http\Controllers\Master\KendaraanController@kendaraan_update')->name('kendaraan_update');
Route::post('/master/kendaraan/delete', 'App\Http\Controllers\Master\KendaraanController@kendaraan_delete')->name('kendaraan_delete');
Route::post('/master/kendaraan/delete', 'App\Http\Controllers\Master\KendaraanController@kendaraan_delete')->name('kendaraan_delete');
Route::get('/master/kendaraan/print/{id}', 'App\Http\Controllers\Master\KendaraanController@kendaraan_print')->name('kendaraan_print');
Route::get('/master/kendaraan/print', 'App\Http\Controllers\Master\KendaraanController@kendaraan_qr')->name('kendaraan_qr');


Route::get('/master/jenis/index', function () {
    return view('masterwms.jenis');
});
Route::get('/master/jenis/list', 'App\Http\Controllers\Master\JenisItemController@jenis_list')->name('jenis_list');
Route::get('/master/jenis/json', 'App\Http\Controllers\Master\JenisItemController@jenis_json')->name('jenis_json');
Route::post('/master/jenis/input', 'App\Http\Controllers\Master\JenisItemController@jenis_input')->name('jenis_input');
Route::get('/master/jenis/edit', 'App\Http\Controllers\Master\JenisItemController@jenis_edit')->name('jenis_edit');
Route::post('/master/jenis/update', 'App\Http\Controllers\Master\JenisItemController@jenis_update')->name('jenis_update');
Route::post('/master/jenis/delete', 'App\Http\Controllers\Master\JenisItemController@jenis_delete')->name('jenis_delete');

/* END */

/* Start::Master WMS */

Route::get('/master/item/index', function () {
    return view('masterwms.item');
});
Route::get('/master/item/list', 'App\Http\Controllers\Master\ItemController@item_list')->name('item_list');
Route::get('/master/item/json', 'App\Http\Controllers\Master\ItemController@item_json')->name('item_json');
Route::post('/master/item/input', 'App\Http\Controllers\Master\ItemController@item_input')->name('item_input');
Route::get('/master/item/edit', 'App\Http\Controllers\Master\ItemController@item_edit')->name('item_edit');
Route::post('/master/item/update', 'App\Http\Controllers\Master\ItemController@item_update')->name('item_update');
Route::post('/master/item/delete', 'App\Http\Controllers\Master\ItemController@item_delete')->name('item_delete');

Route::get('/master/rack/index', function () {
    return view('masterwms.rack');
});
Route::get('/master/rack/list', 'App\Http\Controllers\Master\RackController@rack_list')->name('rack_list');
Route::get('/master/rack/json', 'App\Http\Controllers\Master\RackController@rack_json')->name('rack_json');
Route::post('/master/rack/input', 'App\Http\Controllers\Master\RackController@rack_input')->name('rack_input');
Route::get('/master/rack/edit', 'App\Http\Controllers\Master\RackController@rack_edit')->name('rack_edit');
Route::post('/master/rack/update', 'App\Http\Controllers\Master\RackController@rack_update')->name('rack_update');
Route::post('/master/rack/delete', 'App\Http\Controllers\Master\RackController@rack_delete')->name('rack_delete');
Route::get('/master/rack/print/{id}', 'App\Http\Controllers\Master\RackController@rack_print')->name('rack_print');
Route::get('/master/rack/print', 'App\Http\Controllers\Master\RackController@rack_qr')->name('rack_qr');


Route::get('/master/warehouse/index', function () {
    return view('masterwms.warehouse');
});
Route::get('/master/warehouse/list', 'App\Http\Controllers\Master\WarehouseController@warehouse_list')->name('warehouse_list');
Route::get('/master/warehouse/json', 'App\Http\Controllers\Master\WarehouseController@warehouse_json')->name('warehouse_json');
Route::post('/master/warehouse/input', 'App\Http\Controllers\Master\WarehouseController@warehouse_input')->name('warehouse_input');
Route::get('/master/warehouse/edit', 'App\Http\Controllers\Master\WarehouseController@warehouse_edit')->name('warehouse_edit');
Route::post('/master/warehouse/update', 'App\Http\Controllers\Master\WarehouseController@warehouse_update')->name('warehouse_update');
Route::post('/master/warehouse/delete', 'App\Http\Controllers\Master\WarehouseController@warehouse_delete')->name('warehouse_delete');

Route::get('/master/pallet/index', function () {
    return view('masterwms.pallet');
});
Route::get('/master/pallet/list', 'App\Http\Controllers\Master\PalletController@pallet_list')->name('pallet_list');
Route::get('/master/pallet/json', 'App\Http\Controllers\Master\PalletController@pallet_json')->name('pallet_json');
Route::post('/master/pallet/input', 'App\Http\Controllers\Master\PalletController@pallet_input')->name('pallet_input');
Route::get('/master/pallet/edit', 'App\Http\Controllers\Master\PalletController@pallet_edit')->name('pallet_edit');
Route::post('/master/pallet/update', 'App\Http\Controllers\Master\PalletController@pallet_update')->name('pallet_update');
Route::post('/master/pallet/delete', 'App\Http\Controllers\Master\PalletController@pallet_delete')->name('pallet_delete');
Route::get('/master/pallet/print/{id}', 'App\Http\Controllers\Master\PalletController@pallet_print')->name('pallet_print');
Route::get('/master/pallet/print', 'App\Http\Controllers\Master\PalletController@pallet_qr')->name('pallet_qr');

/* END */

/* Start::Gate */

Route::get('/gate/gate/index', function () {
    return view('master.gateinout');
});
Route::get('/gate/gate-in/list', 'App\Http\Controllers\Get\GetIncontroller@gatein_list')->name('getin_list');
Route::get('/gate/gate-out/list', 'App\Http\Controllers\Get\GetOutcontroller@gateout_list')->name('getout_list');

/* End::Gate */

/* Start::Transaction */

Route::get('/transaction/inbound/index', function () {
    return view('transaction.inbound');
});
Route::get('/transaction/inbound/list', 'App\Http\Controllers\Transaction\InboundController@inbound_list')->name('inbound_list');
Route::get('/transaction/inbound/json', 'App\Http\Controllers\Transaction\InboundController@inbound_json')->name('inbound_json');
Route::post('/transaction/inbound/input', 'App\Http\Controllers\Transaction\InboundController@inbound_input')->name('inbound_input');
Route::get('/transaction/inbound/edit', 'App\Http\Controllers\Transaction\InboundController@inbound_edit')->name('inbound_edit');
Route::post('/transaction/inbound/update', 'App\Http\Controllers\Transaction\InboundController@inbound_update')->name('inbound_update');
Route::post('/transaction/inbound/delete', 'App\Http\Controllers\Transaction\InboundController@inbound_delete')->name('inbound_delete');
Route::get('/transaction/inbound/print/{id}', 'App\Http\Controllers\Transaction\InboundController@inbound_print')->name('inbound_print');
Route::get('/transaction/inbound/print', 'App\Http\Controllers\Transaction\InboundController@inbound_qr')->name('inbound_qr');

/* End::Transaction */

/* Start::List Inbound/Outbond */
Route::get('/transaction/inlist/index', function () {
    return view('transaction.listin');
});
Route::get('/transaction/inlist/list', 'App\Http\Controllers\Transaction\InlistController@inlist_list')->name('inlist_list');

Route::get('/transaction/outlist/index', function () {
    return view('transaction.listout');
});
Route::get('/transaction/outlist/list', 'App\Http\Controllers\Transaction\OutlistController@outlist_list')->name('outlist_list');
/* End::List Inbound/Outbond */

/* Start::Report */
Route::get('/report/palletin/index', function () {
    return view('report.palletin');
});
Route::get('/report/palletin/list', 'App\Http\Controllers\Report\PalletController@palletin_list')->name('palletin_list');

Route::get('/report/palletout/index', function () {
    return view('report.palletout');
});
Route::get('/report/palletout/list', 'App\Http\Controllers\Report\PalletController@palletout_list')->name('palletout_list');

/* End::Report */