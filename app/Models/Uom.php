<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Uom extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'master_uom';

    public $fillable = [
        'id',
        'kode_uom',
        'descr',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
