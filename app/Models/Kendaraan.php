<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kendaraan extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='master_kendaraan';

    public $fillable = [
        'id',
        'no_polisi',
        'type_id',
        'perusahaan_id',
        'personal_id',
        'status',
        'country',
        'keterangan',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
