<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'master_item';

    public $fillable = [
        'id',
        'kode_item',
        'description',
        'jenis_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;

    public function jenis(){

        return  $this->belongsTo('App\Models\JenisItem','jenis_id','id');
    }

}
