<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gate extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'master_gate';

    public $fillable = [
        'id',
        'kode_gate',
        'kelompok',
        'nama',
        'ante_room',
        'printer',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
