<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipeKendaraan extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='tipe_kendaraan';

    public $fillable = [
        'id',
        'kode_tipe',
        'type_kendaran',
        'allias',
        'jumlah_ban',
        'volume_standart',
        'kapasitas',
        'dc_dry',
        'dc_fresh',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
