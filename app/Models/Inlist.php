<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inlist extends Model
{
    use HasFactory;

    protected $table = 'trxstockin';

    public $fillable = [
        'id',
        'inoutid',
        'pallet_id',
        'item_id',
        'nama_ikan',
        'jenis_id',
        'berat',
        'rack_id',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
