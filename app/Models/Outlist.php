<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Outlist extends Model
{
    use HasFactory;

    protected $table = 'trxstockout';

    public $fillable = [
        'id',
        'rack_id',
        'pallet_id',
        'item_id',
        'nama_ikan',
        'jenis_id',
        'berat',
        'inoutid',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
