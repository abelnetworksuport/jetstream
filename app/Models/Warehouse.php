<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='master_warehouse';

    public $fillable = [
        'id',
        'kode_warehouse',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
