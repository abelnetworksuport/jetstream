<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rack extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='master_rack';

    public $fillable = [
        'id',
        'kode_rack',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
