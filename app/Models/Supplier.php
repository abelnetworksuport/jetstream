<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='supplier';

    public $fillable = [
        'id',
        'kode_sup',
        'nama',
        'alamat',
        'kota',
        'hp',
        'fax',
        'email',
        'cp',
        'kelompok',
        'deposit',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
