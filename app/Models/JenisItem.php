<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisItem extends Model
{
    use HasFactory,SoftDeletes;

    protected $table ='jenis_item';

    public $fillable = [
        'id',
        'kode_jenis',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
