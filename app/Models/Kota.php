<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kota extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'master_kota';

    public $fillable = [
        'id',
        'kode_kota',
        'kota',
        'kelompok',
        'tujuan',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
