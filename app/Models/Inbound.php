<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inbound extends Model
{
    use HasFactory;

    protected $table = 'transaction_inbound';

    public $fillable = [
        'id',
        'kode_trx',
        'tujuan',
        'kendaraan',
        'suhu',
        'dari',
        'ke',
        'keterangan',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
