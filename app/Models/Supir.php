<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supir extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'master_supir';

    public $fillable = [
        'id',
        'kode_personal',
        'no_id',
        'nama',
        'alamat',
        'kota',
        'hp',
        'kelompok',
        'perusahaan_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $timestamps = true;
}
