<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Inbound; 
use DB;

class InboundController extends Controller
{
    public function Inbound_list(){
        $Inbound = Inbound::all();
        return Datatables::of($Inbound)->make(true);
    }

    public function Inbound_json(){
        $Inbound = Inbound::all();
        return response()->json($Inbound);
    }

    public function Inbound_input(Request $request){
        $Inbound = new Inbound;
        $Inbound->kode_trx = $request->code;
        $Inbound->tujuan = $request->tujuan;
        $Inbound->kendaraan_id = $request->kendaraanId;
        $Inbound->gate = $request->gate;
        $Inbound->pemesanan = $request->pemesanan;
        $Inbound->suhu = $request->suhu;
        $Inbound->dari = $request->from;
        $Inbound->ke = $request->to;
        $Inbound->keterangan = $request->keterangan;
        $Inbound->save();
    }

    public function Inbound_edit(Request $request){
        $Inbound = Inbound::find($request->id);
        return response()->json($Inbound);
    }

    public function Inbound_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $Inbound = Inbound::find($id);
        $Inbound->kode_trx = $request->code;
        $Inbound->tujuan = $request->tujuan;
        $Inbound->kendaraan_id = $request->kendaraanId;
        $Inbound->pemesanan = $request->pemesanan;
        $Inbound->suhu = $request->suhu;
        $Inbound->dari = $request->from;
        $Inbound->ke = $request->to;
        $Inbound->keterangan = $request->keterangan;
        $Inbound->save();
    }

    public function Inbound_delete(Request $request){
        $id = request()->input('id');
        $Inbound = Inbound::find($id);
        $Inbound->delete();
    }

    public function inbound_print($id){
        $idx=$id;
        // dd($idx);
        $inbound = Inbound::where('id',$idx)->get()
                ->map(function($key){
                    return [
                        'id'                => $key->id,
                        'kendaraan_id'      => $key->kendaraan_id
                    ];
                });
        return view('print.inbound',['no_polsi'=>$inbound ,'idx'=>$idx]);
    }

    public function inbound_qr(Request $request){
        $inbound = Inbound::where('id',$request->id)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'tujuan'        => $key->tujuan,
                        'kendaraan_id'  => $key->kendaraan_id,
                        'kode_trx'      => $key->kode_trx,
                        'suhu'          => $key->suhu,
                        'dari'          => $key->dari,
                        'ke'            => $key->ke,
                        'gate'          => $key->gate
                    ];
                });
        return response()->json($inbound);
    }
}
