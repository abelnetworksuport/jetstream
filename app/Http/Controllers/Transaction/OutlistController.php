<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Outlist;
use DB;

class OutlistController extends Controller
{
    public function outlist_list(){
        $outlist = DB::table('trxstockout AS t')
            ->leftjoin('transaction_inbound AS ti','t.inout_id','=','ti.id')
            ->join('master_item AS i','t.item_id','=','i.id')
            ->join('master_pallet AS p','t.pallet_id','=','p.id')
            ->join('master_rack AS r', 't.rack_id', '=', 'r.id')
            ->select(
                't.id AS id',
                'i.description AS ikan',
                'p.kode_pallet AS pallet',
                'r.kode_rack AS rack',
                't.berat AS weight',
                'ti.suhu AS suhu',
                't.created_at AS created'
            )
            ->get();
            return Datatables::of($outlist)->make(true);
        }
}
