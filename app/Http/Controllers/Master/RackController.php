<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Rack;

class RackController extends Controller
{
    public function rack_list(){
        $rack = Rack::all();
        return Datatables::of($rack)->make(true);
    }

    public function rack_json(){
        $rack = Rack::all();
        return response()->json($rack);
    }

    public function rack_input(Request $request){
        $rack = new Rack;
        $rack->kode_rack = $request->code;
        $rack->description = $request->description;
        $rack->save();
    }

    public function rack_edit(Request $request){
        $rack = Rack::find($request->id);
        return response()->json($rack);
    }

    public function rack_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $rack = Rack::find($id);
        $rack->kode_rack = $request->editCode;
        $rack->description = $request->editDescr;
        $rack->save();
    }

    public function rack_delete(Request $request){
        $id = request()->input('id');
        $rack = Rack::find($id);
        $rack->delete();
    }

    public function rack_print($id){
        $idx=$id;
        // dd($idx);
        $rack = Rack::where('id',$idx)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'kode_rack'     => $key->kode_rack,      
                        'description'   => $key->description
                    ];
                });
        return view('print.rack',['idx'=>$idx]);
    }

    public function rack_qr(Request $request){
        $rack = Rack::where('id',$request->id)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'kode_rack'     => $key->kode_rack,      
                        'description'   => $key->description
                    ];
                });
        return response()->json($rack);
    }
}
