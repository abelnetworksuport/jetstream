<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Uom;

class UomController extends Controller
{
    public function uom_list(){
        $uom = Uom::all();
        return Datatables::of($uom)->make(true);
    }

    public function uom_json(){
        $uom = Uom::all();
        return response()->json($uom);
    }

    public function uom_input(Request $request){
        $uom = new Uom;
        $uom->kode_uom = $request->code;
        $uom->descr = $request->descr;
        $uom->save();
    }

    public function uom_edit(Request $request){
        $uom = Uom::find($request->id);
        return response()->json($uom);
    }

    public function uom_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $uom = Uom::find($id);
        $uom->kode_uom = $request->editCode;
        $uom->description = $request->editDescr;
        $uom->save();
    }

    public function uom_delete(Request $request){
        $id = request()->input('id');
        $uom = Uom::find($id);
        $uom->delete();
    }
}
