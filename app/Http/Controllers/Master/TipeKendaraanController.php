<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\TipeKendaraan;

class TipeKendaraanController extends Controller
{
    //
    public function tipe_list(){
        $tipe = TipeKendaraan::all();
        return Datatables::of($tipe)->make(true);
    }

    public function tipe_json(){
        $tipe = TipeKendaraan::all();
        return response()->json($tipe);
    }

    public function tipe_input(Request $request){
        $tipe = new TipeKendaraan;
        $tipe->kode_tipe = $request->code;
        $tipe->type_kendaran = $request->tipe;
        $tipe->allias = $request->allias;
        $tipe->jumlah_ban = $request->ban;
        $tipe->volume_standart = $request->volume;
        $tipe->kapasitas = $request->kapasitas;
        $tipe->dc_dry = $request->dc_dry;
        $tipe->dc_fresh = $request->dc_fresh;
        $tipe->save();
    }

    public function tipe_edit(Request $request){
        $tipe = TipeKendaraan::find($request->id);
        return response()->json($tipe);
    }
}
