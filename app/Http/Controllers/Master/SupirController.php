<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Supir;
use App\Models\Supplier;
use DB;

class SupirController extends Controller
{
    //
    public function supir_list(){
        $supir = Supir::all();
        return Datatables::of($supir)->make(true);
    }

    public function supir_json(){
        $supir = Supir::all();
        return response()->json($supplier);
    }

    public function supir_input(Request $request){
        $supir = new Supir;
        $supir->kode_personal = $request->code;
        $supir->no_id = $request->no_id;
        $supir->nama = $request->nama;
        $supir->alamat = $request->alamat;
        $supir->kota = $request->kota;
        $supir->hp = $request->hp;
        $supir->kelompok = $request->kelompok;
        $supir->perusahaan_id = $request->perusahaanId;
        $supir->save();
    }

    public function supir_edit(Request $request){
        $supir = Supir::find($request->id);
        return response()->json($supir);
    }

    public function supir_print($id){
        $idx=$id;
        $supir = Supir::where('id',$idx)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                    ];
                });
        return view('print.tamu',['id'=>$supir ,'idx'=>$idx]);
    }

    public function supir_qr(Request $request){
        $supir = Supir::where('id',$request->id)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'kode_personal' => $key->kode_personal,
                        'no_id'         => $key->no_id,
                        'nama'          => $key->nama,
                        'kota'          => $key->kota,
                        'hp'            => $key->hp,
                    ];
                });
        return response()->json($supir);
    }
}
