<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Kota;

class KotaController extends Controller
{
    //
    public function kota_list(){
        $kota = Kota::all();
        return Datatables::of($kota)->make(true);
    }

    public function kota_json(){
        $kota = Kota::all();
        return response()->json($kota);
    }

    public function kota_input(Request $request){
        $kota = new Kota;
        $kota->kode_kota = $request->code;
        $kota->kota = $request->kota;
        $kota->kelompok = $request->kelompok;
        $kota->tujuan = $request->tujuan;
        $kota->save();
    }

    public function kota_edit(Request $request){
        $kota = Kota::find($request->id);
        return response()->json($kota);
    }

    public function kota_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $kota = Kota::find($id);
        $kota->kode_kota = $request->editCode;
        $kota->kota = $request->editKota;
        $kota->kelompok = $request->editKelompok;
        $kota->tujuan = $request->editTujuan;
        $gate->save();
    }

    public function kota_delete(Request $request){
        $id = request()->input('id');
        $kota = Kota::find($id);
        $kota->delete();
    }
}
