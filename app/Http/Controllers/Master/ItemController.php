<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Item;
use App\Models\JenisItem;

class ItemController extends Controller
{
    public function item_list(){
        $item = Item::all();
        return Datatables::of($item)->make(true);
    }

    public function item_json(){
        $item = Item::all();
        return response()->json($item);
    }

    public function item_input(Request $request){
        $item = new Item;
        $item->kode_item = $request->code;
        $item->description = $request->description;
        $item->jenis_id = $request->jenisId;
        $item->save();
    }

    public function item_edit(Request $request){
        $item = Item::find($request->id);
        return response()->json($item);
    }

    public function item_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $item = Item::find($id);
        $item->kode_item = $request->editCode;
        $item->description = $request->editDescr;
        $item->jenis_id = $request->editJenisId;
        $item->save();
    }

    public function item_delete(Request $request){
        $id = request()->input('id');
        $item = Item::find($id);
        $item->delete();
    }
}
