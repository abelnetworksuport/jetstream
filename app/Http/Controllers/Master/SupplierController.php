<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Supplier;

class SupplierController extends Controller
{
    //
    public function supplier_list(){
        $supplier = Supplier::all();
        return Datatables::of($supplier)->make(true);
    }

    public function supplier_json(){
        $supplier = Supplier::all();
        return response()->json($supplier);
    }

    public function supplier_input(Request $request){
        $supplier = new Supplier;
        $supplier->kode_sup = $request->code;
        $supplier->nama = $request->nama;
        $supplier->alamat = $request->alamat;
        $supplier->kota = $request->kota;
        $supplier->hp = $request->kelompok;
        $supplier->fax = $request->fax;
        $supplier->email = $request->email;
        $supplier->cp = $request->cp;
        $supplier->kelompok = $request->kelompok;
        $supplier->deposit = $request->limit;
        $supplier->save();
    }

    public function supplier_edit(Request $request){
        $supplier = Supplier::find($request->id);
        return response()->json($supplier);
    }
}
