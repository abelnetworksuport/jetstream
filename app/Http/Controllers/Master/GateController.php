<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Gate;


class GateController extends Controller
{
    //
    public function gate_list(){
        $gate = Gate::all();
        return Datatables::of($gate)->make(true);
    }

    public function gate_json(){
        $gate = Gate::all();
        return response()->json($gate);
    }

    public function gate_input(Request $request){
        $gate = new Gate;
        $gate->kode_gate = $request->code;
        $gate->kelompok = $request->kelompok;
        $gate->nama = $request->nama;
        $gate->ante_room = $request->ante_room;
        $gate->printer = $request->printer;
        $gate->status = $request->status;
        $gate->save();
    }

    public function gate_edit(Request $request){
        $gate = Gate::find($request->id);
        return response()->json($gate);
    }

    public function gate_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $gate = Gate::find($id);
        $gate->kode_gate = $request->editCode;
        $gate->kelompok = $request->editKelompok;
        $gate->nama = $request->editNama;
        $gate->ante_room = $request->editRoom;
        $gate->printer = $request->editPrinter;
        $gate->status = $request->editStatus;
        $gate->save();
    }

    public function gate_delete(Request $request){
        $id = request()->input('id');
        $gate = Gate::find($id);
        $gate->delete();
    }
}
