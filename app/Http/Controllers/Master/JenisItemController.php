<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\JenisItem;

class JenisItemController extends Controller
{
    public function jenis_list(){
        $jenis = JenisItem::all();
        return Datatables::of($jenis)->make(true);
    }

    public function jenis_json(){
        $jenis = JenisItem::all();
        return response()->json($jenis);
    }

    public function jenis_input(Request $request){
        $jenis = new JenisItem;
        $jenis->kode_jenis = $request->code;
        $jenis->description = $request->description;
        $jenis->save();
    }

    public function jenis_edit(Request $request){
        $jenis = JenisItem::find($request->id);
        return response()->json($jenis);
    }

    public function jenis_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $jenis = JenisItem::find($id);
        $jenis->kode_jenis = $request->editCode;
        $jenis->description = $request->editDescr;
        $jenis->save();
    }

    public function jenis_delete(Request $request){
        $id = request()->input('id');
        $jenis = JenisItem::find($id);
        $jenis->delete();
    }

}
