<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Pallet;

class PalletController extends Controller
{
    public function pallet_list(){
        $pallet = Pallet::all();
        return Datatables::of($pallet)->make(true);
    }

    public function pallet_json(){
        $pallet = Pallet::all();
        return response()->json($pallet);
    }

    public function pallet_input(Request $request){
        $pallet = new Pallet;
        $pallet->kode_pallet = $request->code;
        $pallet->description = $request->description;
        $pallet->weight = $request->weight;
        $pallet->save();
    }

    public function pallet_edit(Request $request){
        $pallet = Pallet::find($request->id);
        return response()->json($pallet);
    }

    public function pallet_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $pallet = Pallet::find($id);
        $pallet->kode_pallet = $request->editCode;
        $pallet->description = $request->editDescr;
        $pallet->weight = $request->editWeight;
        $pallet->save();
    }

    public function pallet_delete(Request $request){
        $id = request()->input('id');
        $pallet = Pallet::find($id);
        $pallet->delete();
    }

    public function pallet_print($id){
        $idx=$id;
        // dd($idx);
        $pallet = Pallet::where('id',$idx)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                    ];
                });
        return view('print.pallet',['idx'=>$idx]);
    }

    public function pallet_qr(Request $request){
        $pallet = Pallet::where('id',$request->id)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'kode_pallet'     => $key->kode_pallet,      
                        'description'   => $key->description
                    ];
                });
        return response()->json($pallet);
    }
}
