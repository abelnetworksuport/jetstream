<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Kendaraan;
use App\Models\TipeKendaraan;
use App\Models\Supplier;
use DB;

class KendaraanController extends Controller
{
    //
    public function kendaraan_list(){
        $kendaraan = DB::table('master_kendaraan AS mk')
            ->join('tipe_kendaraan AS tk', 'mk.type_id', '=' ,'tk.id')
            ->leftjoin('supplier AS s', 'mk.perusahaan_id','=','s.id')
            ->leftjoin('master_supir AS ms', 'mk.personal_id', '=', 'ms.id')
            ->select(
                'mk.id AS id',
                'mk.no_polisi AS no_polisi',
                'tk.kode_tipe AS type',
                's.nama AS supplier',
                'ms.nama AS personal'
            )
            ->get();
        return Datatables::of($kendaraan)->make(true);
    }

    public function kendaraan_json(){
        $kendaraan = Kendaraan::all();
        return response()->json($supplier);
    }

    public function kendaraan_input(Request $request){
        // dd($request);
        $kendaraan = new Kendaraan;
        $kendaraan->no_polisi = $request->no;
        $kendaraan->tahun = $request->tahun;
        $kendaraan->type_id = $request->type_id;
        $kendaraan->perusahaan_id = $request->perusahaanId;
        $kendaraan->personal_id = $request->personal_id;
        $kendaraan->status = $request->status;
        $kendaraan->country = $request->country;
        $kendaraan->keterangan = $request->fax;
        $kendaraan->save();
    }

    public function kendaraan_edit(Request $request){
        $id= request()->input('id');
        $kendaraan = DB::table('master_kendaraan AS mk')
            ->join('tipe_kendaraan AS tk', 'mk.type_id', '=' ,'tk.id')
            ->leftjoin('supplier AS s', 'mk.perusahaan_id','=','s.id')
            ->leftjoin('master_supir AS ms', 'mk.personal_id', '=', 'ms.id')
            ->where('mk.id',$id)
            ->select(
                'mk.id AS mkId',
                'mk.no_polisi AS no_polisi',
                'tk.kode_tipe AS type',
                'tk.type_kendaran AS tipe',
                's.kode_sup AS ks',
                's.nama AS supplier',
                'ms.kode_personal AS kp',
                'ms.nama AS personal'
            )
            ->get();
        return response()->json($kendaraan);
    }

    public function kendaraan_print($id){
        $idx=$id;
        // dd($idx);
        $kendaraan = Kendaraan::where('id',$idx)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'no_polsi'      => $key->no_polsi
                    ];
                });
        return view('print.kendaraan',['no_polsi'=>$kendaraan ,'idx'=>$idx]);
    }

    public function kendaraan_qr(Request $request){
        $kendaraan = Kendaraan::where('id',$request->id)->get()
                ->map(function($key){
                    return [
                        'id'            => $key->id,
                        'no_polisi'      => $key->no_polisi,
                        'tahun'         => $key->tahun
                    ];
                });
        return response()->json($kendaraan);
    }
}
