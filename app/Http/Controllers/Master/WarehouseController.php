<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Warehouse;

class WarehouseController extends Controller
{
    public function warehouse_list(){
        $warehouse = Warehouse::all();
        return Datatables::of($warehouse)->make(true);
    }

    public function warehouse_json(){
        $warehouse = Warehouse::all();
        return response()->json($warehouse);
    }

    public function warehouse_input(Request $request){
        $warehouse = new Warehouse;
        $warehouse->kode_warehouse = $request->code;
        $warehouse->description = $request->description;
        $warehouse->save();
    }

    public function warehouse_edit(Request $request){
        $warehouse = Warehouse::find($request->id);
        return response()->json($warehouse);
    }

    public function warehouse_update(Request $request){
        // dd($request);
        $id = request()->input('editId');
        $warehouse = Warehouse::find($id);
        $warehouse->kode_warehouse = $request->editCode;
        $warehouse->description = $request->editDescr;
        $warehouse->save();
    }

    public function warehouse_delete(Request $request){
        $id = request()->input('id');
        $warehouse = Warehouse::find($id);
        $warehouse->delete();
    }
}
