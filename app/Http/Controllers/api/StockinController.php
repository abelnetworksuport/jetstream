<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Item;
use App\Models\JenisItem;
use App\Models\Rack;
use App\Models\Pallet;
use DB;
use App\Models\Gate;

class StockinController extends Controller
{
    //
    public function cari_inbound(Request $request){
        $kodein     = $request->input('kode');
        //dd($request->all());

        if ($kodein != null ) {
            # code...
            
            $kendaraan = DB::table('transaction_inbound')->where('kode_trx',$kodein)->first();
            //dd($kendaraan);
            if($kendaraan){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'kendaraan' => $kendaraan,                    
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'kendaraaan' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'kendaraan' => $kendaraan,  
                                  
                ]
            ],401);
        }
    }
    public function cari_item(Request $request){
        $item = $request->input('kode_item');
        //dd($item);
        if ($item != null ) {
            # code...
            
            $dataItem = Item::where('kode_item', $item)->first();
                
            
            //dd($dataItem);
            if($dataItem){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'dataitem' => $dataItem,
                        'jenisitem' => $dataItem->jenis->description                  
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'dataitem' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'item' => $item,  
                                  
                ]
            ],401);
        }
    }
    public function cari_pallet(Request $request){
        $pallet = $request->input('scan_pallet');
        //dd($pallet);
        if ($pallet != null ) {
            # code...
            
            $datapallet = Pallet::where('kode_pallet', $pallet)->first();

            //dd($datapallet);
            if($datapallet){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'datapallet' => $datapallet,                
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'datapallet' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'datapallet' => $datapallet,  
                                  
                ]
            ],401);
        }
    }

    public function cari_rack(Request $request){
        $rack = $request->input('scan_rack');
        //dd($rack);
        if ($rack != null ) {
            # code...
            
            $datarack = Rack::where('kode_rack', $rack)->first();

            //dd($dataItem);
            if($datarack){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'datarack' => $datarack,                
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'datarack' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'datarack' => $datarack,  
                                  
                ]
            ],401);
        }
    }
    public function cari_gate(Request $request){
        $gate = $request->input('scan_gate');
        //dd($rack);
        if ($gate != null ) {
            # code...
            
            $datagate = Gate::where('kode_gate', $gate)->first();

            //dd($dataItem);
            if($datagate){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'datagate' => $datagate,                
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'datagate' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'datagate' => $datagate,  
                                  
                ]
            ],401);
        }
    }

    public function post_trxin(Request $request){
        $inbond = $request->input('scan_inbound');
        $pallet = $request->input('scan_pallet');
        $item = $request->input('scan_item');
        $jenis = $request->input('scan_jenis');
        $berat = $request->input('berat');
        $suhu = $request->input('suhu');
        $rack = $request->input('scan_rack');

        if ($inbond != null ) {
            $inbonId = DB::table('transaction_inbound')->select('id')->where('kode_trx',$inbond)->first();
            //dd($inbonId->id);
            $palletId = DB::table('master_pallet')->select('id')->where('kode_pallet',$pallet)->first();
            $itemId = DB::table('master_item')->select('id')->where('kode_item',$item)->first();
            $rackId = DB::table('master_rack')->select('id')->where('kode_rack',$rack)->first();
            $jenis_Id = DB::table('jenis_item')->select('id')->where('kode_jenis',$jenis)->first();
            

            //dd($jenis_Id->id);
            
            $trxin = DB::table('trxstockin')->insertGetId([
                
                'inout_id'  => $inbonId->id,
                'pallet_id' => $palletId->id,
                'item_id'       => $itemId->id,
                'jenis_id'       => $jenis_Id->id,
                'berat'         => $berat,
                'suhu'          => $suhu,
                'rack_id'       => $rackId->id,
                "created_at" =>  Carbon::now(), 
                "updated_at" => Carbon::now(),
                
            ]);
           
            return response()->json([
                'success' =>true,
                'meesage' => 'data sucess',
                'data'     => [
                    'trxin' => $trxin,                    
                    ]
            ],200);

        }else{
            return response()->json([
                'success' =>false,
                'meesage' => 'data sucess',
                'data'     => [
                    'trxin' => $trxin,                    
                    ]
            ],400);
            
        }
    }
    public function post_trxout(Request $request){
        $outbond = $request->input('scan_outbound');
        $rack = $request->input('scan_rack');
        $pallet = $request->input('scan_pallet');
        $item = $request->input('scan_item');
        $jenis = $request->input('scan_jenis');
        $berat = $request->input('berat');
        $gate = $request->input('scan_gate');
        
        

        if ($outbond != null ) {
            $outbond = DB::table('transaction_inbound')->select('id')->where('kode_trx',$outbond)->first();
            //dd($inbonId->id);
            $palletId = DB::table('master_pallet')->select('id')->where('kode_pallet',$pallet)->first();
            $itemId = DB::table('master_item')->select('id')->where('kode_item',$item)->first();
            $rackId = DB::table('master_rack')->select('id')->where('kode_rack',$rack)->first();
            $jenis_Id = DB::table('jenis_item')->select('id')->where('kode_jenis',$jenis)->first();
            $gateId = DB::table('master_gate')->select('id')->where('kode_gate',$gate)->first();

            //dd($jenis_Id->id);
            
            $trxin = DB::table('trxstockout')->insertGetId([
                
                'inout_id'  => $outbond->id,
                'pallet_id' => $palletId->id,
                'item_id'       => $itemId->id,
                'jenis_ikan'       => $jenis_Id->id,
                'berat'         => $berat,
                'rack_id'       => $rackId->id,
                'gate_id'       => $gateId->id,
                "created_at" =>  Carbon::now(), 
                "updated_at" => Carbon::now(),
                
            ]);
           
            return response()->json([
                'success' =>true,
                'meesage' => 'data sucess',
                'data'     => [
                    'trxin' => $trxin,                    
                    ]
            ],200);

        }else{
            return response()->json([
                'success' =>false,
                'meesage' => 'data sucess',
                'data'     => [
                    'trxin' => $trxin,                    
                    ]
            ],400);
            
        }
    }

    
}
