<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class GetinController extends Controller
{
    //
    public function tampil_getin(){
        $kendaraan = DB::table('master_kendaraan')->where('status_get',2)->get()
        ->map(function($key){
            return [
                'nopolis' => $key->no_polisi,
                'status' => 'GET-IN',
                'perusahaan' => $key->perusahaan_id
            ];
        });
        return response()->json([
            'success' =>true,
            'meesage' => 'data sucess',
            'data'     => [
                'kendaraan' => $kendaraan,                    
            ]
        ],200);
    }
    public function cari_getin(Request $request){
        $nopolis     = $request->input('nopolisi');
        //dd($request->all());

        if ($nopolis != null ) {
            # code...
            
            $kendaraan = DB::table('master_kendaraan')->where('no_polisi',$nopolis)->where('status_get',1)->first();
            //dd($kendaraan);
            if($kendaraan){
                return response()->json([
                    'success' =>true,
                    'meesage' => 'data sucess',
                    'data'     => [
                        'kendaraan' => $kendaraan,                    
                    ]
                ],200);
            }else{
                return response()->json([
                    'success' =>false,
                    'meesage' => 'data not found',
                    'data'     => [
    
                        'kendaraaan' => '',                    
                    ]
                ],201);
            }


        }else{
            //dd( $noshipmen);
            
            //dd($kendaraan);
            return response()->json([
                'success' =>false,
                'meesage' => 'data null',
                'data'     => [

                    'nopolisi' => $nopolis,  
                                  
                ]
            ],401);
        }
    }

    public function post_getin(Request $request){
        $nopolis     = $request->input('nopolisi');
        $suhu     = $request->input('suhu');
       

        if ($nopolis != null ) {
            $cariId = DB::table('master_kendaraan')->select('id')->where('no_polisi',$nopolis)->where('status_get',1)->first();
            //dd($cariId->id);
            $kendaraan = DB::table('getin')->insertGetId([
                
                'mkendaraan_id' => $cariId->id,
                'suhu'          => $suhu,
                'user_id'       => 1,
                "created_at" =>  Carbon::now(), 
                "updated_at" => Carbon::now(),
                
            ]);
            $masterkendaraan = DB::table('master_kendaraan')->where('id',$cariId->id)->update([
                //'transaction_idddddddddddd' => $trnsaksi,
                'status_get' => 2,
            ]);
            return response()->json([
                'success' =>true,
                'meesage' => 'data sucess',
                'data'     => [
                    'kendaraan' => $kendaraan,                    
                    ]
            ],200);

        }else{
            return response()->json([
                'success' =>false,
                'meesage' => 'data sucess',
                'data'     => [
                    'barang' => $kendaraan,                    
                    ]
            ],400);
            
        }
    }
}
