<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Auth;
use Validator;
use Carbon\Carbon;


class UserController extends Controller
{
    //
    public $successStatus = 200;
    public function login(Request $request){

        
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = $user = User::where('email',request('email'))->first();
            //dd($user);

            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(
                [
                    'auth' => $user,
                    'success' => $success
                ],
                 $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }

    }
    public function details()
    {
        $user = Auth::user();
        //dd($user);
        //$user = User::all();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
