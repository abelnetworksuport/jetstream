<?php

namespace App\Http\Controllers\Get;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Gate;
use DB;

class GetOutcontroller extends Controller
{
    //
    public function gateout_list(){
        $gate = DB::table('master_kendaraan')->where('status_get',3)->get()
        ->map(function($key){
            return [
                'nopolis' => $key->no_polisi,
                'status' => 'GET-IN',
                'perusahaan' => $key->perusahaan_id,
                'date'    => $key->created_at
            ];
        });
        return Datatables::of($gate)->make(true);
    }
}
