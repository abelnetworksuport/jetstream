<?php

namespace App\Http\Controllers\Get;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Gate;
use DB;

class GetIncontroller extends Controller
{
    //
    public function gatein_list(){
        $gate = DB::table('master_kendaraan')->get()
        ->map(function($key){
            return [
                'id'      => $key->id,
                'nopolis' => $key->no_polisi,
                'status' => $key->status_get,
                'perusahaan' => $key->perusahaan_id,
                'date'    => Carbon::parse($key->created_at)->format('Y-m-d'),
            ];
        });
        return Datatables::of($gate)->make(true);
    }
}
