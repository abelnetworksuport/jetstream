<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Inlist;
use DB;

class PalletController extends Controller
{
    public function palletin_list(){
        $palletin = DB::table('trxstockin AS t')
            ->leftjoin('transaction_inbound AS ti','t.inout_id','=','ti.id')
            ->join('master_item AS i','t.item_id','=','i.id')
            ->join('master_pallet AS p','t.pallet_id','=','p.id')
            ->join('master_rack AS r', 't.rack_id', '=', 'r.id')
            ->leftjoin('master_kendaraan AS mk','ti.kendaraan_id','=','mk.id')
            ->join('tipe_kendaraan AS tk', 'mk.type_id', '=' ,'tk.id')
            ->leftjoin('supplier AS s', 'mk.perusahaan_id','=','s.id')
            ->select(
                't.id AS id',
                'i.description AS ikan',
                'p.kode_pallet AS pallet',
                'r.kode_rack AS rack',
                't.berat AS weight',
                'ti.suhu AS suhu',
                'ti.gate AS gate',
                'tk.kode_tipe AS type',
                's.nama AS supplier',
                'mk.no_polisi AS kendaraan',
                't.berat AS berat',
                't.created_at AS date'
            )
            ->get();
        return Datatables::of($palletin)->make(true);
    }

    public function palletout_list(){
        $palletin = DB::table('trxstockout AS t')
            ->leftjoin('transaction_inbound AS ti','t.inout_id','=','ti.id')
            ->join('master_item AS i','t.item_id','=','i.id')
            ->join('master_pallet AS p','t.pallet_id','=','p.id')
            ->join('master_rack AS r', 't.rack_id', '=', 'r.id')
            ->leftjoin('master_kendaraan AS mk','ti.kendaraan_id','=','mk.id')
            ->join('tipe_kendaraan AS tk', 'mk.type_id', '=' ,'tk.id')
            ->leftjoin('supplier AS s', 'mk.perusahaan_id','=','s.id')
            ->select(
                't.id AS id',
                'i.description AS ikan',
                'p.kode_pallet AS pallet',
                'r.kode_rack AS rack',
                't.berat AS weight',
                'ti.suhu AS suhu',
                'ti.gate AS gate',
                'tk.kode_tipe AS type',
                's.nama AS supplier',
                'mk.no_polisi AS kendaraan',
                't.berat AS berat',
                't.created_at AS date'
            )
            ->get();
        return Datatables::of($palletin)->make(true);
    }
}
