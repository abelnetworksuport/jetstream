@extends('template.layout')


@section ('css')
<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

<!--end::Global Theme Styles -->
<link href="{{ asset('assets/plugins/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<!--begin::Layout Skins(used by all pages) -->
<link href="{{ asset('/assets/vendors/datatables/datatables.bundle.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/mk-notifications.css') }}" rel="stylesheet"  type="text/css" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/themes/mk-theme-more.css') }}" rel="stylesheet"  type="text/css" />
<style type="text/css">
    .notactive {
        pointer-events: none;
        cursor: default;
    }
</style>
@endsection

@section ('content-header')
<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">
        MASTER </h3>
    <span class="kt-subheader__separator kt-hidden"></span>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="#" class="kt-subheader__breadcrumbs-link">Kendaraan</a>
        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
    </div>
</div>
<div class="kt-subheader__toolbar">

</div>
@endsection


@section ('content-body')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Kendaraan
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-right" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_1_tab_content"
                            role="tab" id="panel-data">
                            <i class="flaticon-lifebuoy"></i> DATA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_tab_content" role="tab"
                            id="panel-tambah">
                            <i class="flaticon-lifebuoy"></i> INSERT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_tab_content" role="tab"
                            id="panel-ubah">
                            <i class="flaticon-lifebuoy"></i> EDIT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_4_tab_content" role="tab"
                            id="panel-detail">
                            <i class="flaticon-lifebuoy"></i> DETAIL
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="kt_portlet_base_demo_1_tab_content" role="tabpanel">
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_1" id="tambah">INSERT</button>
                                        {{-- <button type="button" class="btn btn-outline-success float-right btn-sm" data-toggle="kt-tooltip" data-placement="bottom" title="Import File"><i class="fa fa-file-import"></i></button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="kt-portlet__body">
                        <!--begin: Datatable -->
                        <table class="table table-sm table-striped- table-bordered table-hover table-checkable" id="listKendaraan">
                            <thead>
                                <tr>
                                    <th width=5%>NO</th>
                                    <th>NO POLISI</th>
                                    <th>TYPE</th>
                                    <th>PERUSAHAAN</th>
                                    <th width=25%>ACTION</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable -->
                    </div>
                </div>

                <!-- Input data -->
                <div class="tab-pane" id="kt_portlet_base_demo_2_tab_content" role="tabpanel">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" action="#" id="formInput">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="id" name="id" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">No Polisi<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="no" id="no" placeholder="No Polisi " oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Tahun Kendaraan<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="tahun" id="tahun" placeholder="Tahun Kendaraan" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Type<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="text" name="type_id" id="typeId" class="form-control form-control-sm idtype">
                                                <input type="text" id="codeDescr" class="form-control form-control-sm codeDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariTipe" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalTipe"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="typeDescr" class="form-control form-control-sm typeDescr" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Jumlah Ban<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="ban" id="ban" placeholder="Jumlah Ban" oninput="this.value = this.value.toUpperCase()" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Volume Standart<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="vol_std" id="vol_std" placeholder="Volume Standart" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <label class="col-lg-2 col-form-label">Volume Kendaraan(PxLxT)<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-2 col-sm-4">
                                            <input type="number" class="form-control form-control-sm" name="p" id="p" placeholder="P" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>X</b>
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="l" id="l" placeholder="L" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>X</b>
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="t" id="t" placeholder="T" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Volume Toleransi<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-2 col-sm-4">
                                            <input type="number" class="form-control form-control-sm" name="vol_tol" id="vol_tol" placeholder="" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>%</b>
                                        <div class="col-lg-2 col-sm-12">
                                            
                                        </div>
                                        <b>=</b> 
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="kapasitas" id="kapasitas" placeholder="Kapasitas" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Perusahaan<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="perusahaanId" id="perusahaanId" class="form-control form-control-sm idperusahaan">
                                                <input type="text" id="perusahaanDescr" class="form-control form-control-sm perusahaanDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariPerusahaan" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalPerusahaan"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <b></b>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="namaSup" class="form-control form-control-sm namaSup" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Supir</label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="supirId" id="supirId" class="form-control form-control-sm idsupir">
                                                <input type="text" id="kodeSupir" class="form-control form-control-sm kodeSupir" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariSupir" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalPersonal"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <b></b>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="supirDescr" class="form-control form-control-sm supirDescr" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <label class="col-lg-2 col-form-label">Status Active<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <select name="status" id="status" class="form-control form-control-sm">
                                                <option value="">--PILIH--</option>
                                                <option value="1">YA</option>
                                                <option value="0">TIDAK</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">in Country ?<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <select name="country" id="country" class="form-control form-control-sm">
                                                <option value="">--PILIH--</option>
                                                <option value="1">YA</option>
                                                <option value="0">TIDAK</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Keterangan</label>
                                        <div class="col-lg-6 col-sm-12">
                                            <input type="text" name="keterangan" id="keterangan" class="form-control form-control-sm" placeholder="NOTE">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">INSERT</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
                <!-- input data -->

                <!-- Ubah Data -->
                <div class="tab-pane" id="kt_portlet_base_demo_3_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formUpdate">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="editId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">No Polisi<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="no" id="no" placeholder="No Polisi " oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Tahun Kendaraan<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="tahun" id="tahun" placeholder="Tahun Kendaraan" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Type<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="text" name="type_id" id="typeId" class="form-control form-control-sm idtype">
                                                <input type="text" id="codeDescr" class="form-control form-control-sm codeDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariTipe" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalTipe"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="typeDescr" class="form-control form-control-sm typeDescr" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Jumlah Ban<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="ban" id="ban" placeholder="Jumlah Ban" oninput="this.value = this.value.toUpperCase()" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Volume Standart<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="vol_std" id="vol_std" placeholder="Volume Standart" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <label class="col-lg-2 col-form-label">Volume Kendaraan(PxLxT)<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-2 col-sm-4">
                                            <input type="number" class="form-control form-control-sm" name="p" id="p" placeholder="P" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>X</b>
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="l" id="l" placeholder="L" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>X</b>
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="t" id="t" placeholder="T" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Volume Toleransi<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-2 col-sm-4">
                                            <input type="number" class="form-control form-control-sm" name="vol_tol" id="vol_tol" placeholder="" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                        <b>%</b>
                                        <div class="col-lg-2 col-sm-12">
                                            
                                        </div>
                                        <b>=</b> 
                                        <div class="col-lg-2 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="kapasitas" id="kapasitas" placeholder="Kapasitas" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Perusahaan<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="perusahaanId" id="perusahaanId" class="form-control form-control-sm idperusahaan">
                                                <input type="text" id="perusahaanDescr" class="form-control form-control-sm perusahaanDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariPerusahaan" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalPerusahaan"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <b></b>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="namaSup" class="form-control form-control-sm namaSup" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Supir</label>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="supirId" id="supirId" class="form-control form-control-sm supirId">
                                                <input type="text" id="kodeSupir" class="form-control form-control-sm kodeSupir" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariSupir" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalPersonal"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <b></b>
                                        <div class="col-lg-3 col-sm-12">
                                            <input type="text" id="supirDescr" class="form-control form-control-sm supirDescr" placeholder="SEARCH"  readonly>
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <label class="col-lg-2 col-form-label">Status Active<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <select name="status" id="status" class="form-control form-control-sm">
                                                <option value="">--PILIH--</option>
                                                <option value="1">YA</option>
                                                <option value="0">TIDAK</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">in Country ?<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-3 col-sm-12">
                                            <select name="country" id="country" class="form-control form-control-sm">
                                                <option value="">--PILIH--</option>
                                                <option value="1">YA</option>
                                                <option value="0">TIDAK</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Keterangan</label>
                                        <div class="col-lg-6 col-sm-12">
                                            <input type="text" name="keterangan" id="keterangan" class="form-control form-control-sm" placeholder="NOTE">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">UPDATE</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Ubah Data -->

                <!-- Detail -->
                <div class="tab-pane" id="kt_portlet_base_demo_4_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formDetail">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="detailId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">CODE<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editCode" id="detailCode" placeholder="Code" oninput="this.value = this.value.toUpperCase()" readonly>
                                        </div>
                                        <label class="col-lg-2 col-form-label">DESCRIPTION<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editDescr" id="detailDescr" placeholder="Description" oninput="this.value = this.value.toUpperCase()" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="button" class="btn btn-secondary kembali">CLOSE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Detailll -->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal Tipe -->
<div class="modal fade" id="modalTipe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Tipe Kendaraan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="listTipe">
                        <thead>
                            <tr>
                                <th width=5%>NO</th>
                                <th>KODE TYPE</th>
                                <th>TYPE KENDARAAN</th>
                                <th>TYPE SINGKAT</th>
                                <th width=20%>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Tipe-->

<!--begin::Modal Perusahaan -->
<div class="modal fade" id="modalPerusahaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST Perusahaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="listSupplier">
                        <thead>
                            <tr>
                                <th width=5%>NO</th>
                                <th>KODE PERUSAHAAN</th>
                                <th>NAMA PERUSAHAAN</th>
                                <th>KELOMPOK</th>
                                <th width=20%>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Perusahaan-->

<!--begin::Modal Perusahaan -->
<div class="modal fade" id="modalPersonal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST Perusahaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="listPersonal">
                        <thead>
                            <tr>
                                <th width=5%>NO</th>
                                <th>KODE PERUSAHAAN</th>
                                <th>NAMA PERUSAHAAN</th>
                                <th>KELOMPOK</th>
                                <th width=20%>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Perusahaan-->

@endsection



@section('js')
<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/mknotif/js/mk-notifications.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
@endsection

@section ('jscustom')
<script>

    var listKendaraan;

    $(document).ready(function () {    
        mkNotifications({
            positionX: "right",
            positionY: "top"
        });
        
        $('#tambah').click(function () {
            tampilTambahTab();
        });

        $('.kembali').click(function () {
            tampilDataTab();
        });
        hideAwalTab();

        listKendaraan = $('#listKendaraan').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('kendaraan_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'no_polisi', name: 'no_polisi' },
                { data: 'type', name: 'type' },
                { data: 'supplier', name: 'supplier' },
                { data: 'id',
                    render: function (data, type, row) {
                        // console.log(type);
                        let url = '{{ url("/master/kendaraan/print/id") }}';
                            url = url.replace('id', row.id);
                        let buttonDetail = '<button type="button" class="btn btn-default btn-sm "  id="lihat" onclick=buttonDetail(' + data + '); '+row.r+'>Detail</button>'
                        let buttonEdit = '<button type="button" class="btn btn-default btn-sm ubah" id="ubah" data-toggle="modal" data-target="#kt_modal_edit"  onclick="buttonEdit(\''+data+'\');" '+row.u+'>Update</button>'
                        let buttonDelete = '<button type="button" class="btn btn-default btn-sm hapus" onclick="buttonHapus(\''+data+'\');" '+row.d+'>Delete</button>'
                        let buttonPrint = '<a href="'+url+'" class="btn btn-default btn-sm">Print</a>'
                        return buttonPrint+buttonDetail+buttonEdit+buttonDelete;
                    }
                }
            ]
        });

    });
    
    listTipe = $('#listTipe').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('tipe_list')}}",
        columns: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'kode_tipe', name: 'kode_tipe' },
            { data: 'type_kendaran', name: 'type_kendaran' },
            { data: 'allias', name: 'allias' },
            { data: 'id',
                render: function (data, type, row) {
                    // console.table(type);
                    let doAddTipe =
                        '<button type="button" class="btn btn-default btn-sm"  onclick=doAddTipe(' +
                        data + '); ><i class="fa fa-plus"></i></button>';
                    return doAddTipe;
                }
            }
        ]
    });

    function doAddTipe(idx) {
        // console.log(idx);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route ('tipe_edit') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                // console.log(datax);
                $('#typeId').val(datax.id);
                $('#codeDescr').val(datax.kode_tipe);
                $('#typeDescr').val(datax.type_kendaran);
                $('#ban').val(datax.jumlah_ban);
                $('#vol_std').val(datax.volume_standart);
                $('#kapasitas').val(datax.kapasitas);
                $('.close').click();
            }
        });
    }

    listSupplier = $('#listSupplier').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('supplier_list')}}",
        columns: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'kode_sup', name: 'kode_sup' },
            { data: 'nama', name: 'nama' },
            { data: 'kelompok', name: 'kelompok' },
            { data: 'id',
                render: function (data, type, row) {
                    // console.table(type);
                    let doAddSup =
                        '<button type="button" class="btn btn-default btn-sm"  onclick=doAddSup(' +
                        data + '); ><i class="fa fa-plus"></i></button>';
                    return doAddSup;
                }
            }
        ]
    });

    function doAddSup(idx) {
        // console.log(idx);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route ('supplier_edit') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                // console.log(datax);
                $('.idperusahaan').val(datax.id);
                $('.perusahaanDescr').val(datax.kode_sup);
                $('.namaSup').val(datax.nama);
                $('.close').click();
            }
        });
    }

    listPersonal = $('#listPersonal').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('supir_list')}}",
        columns: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'kode_personal', name: 'kode_personal' },
            { data: 'nama', name: 'nama' },
            { data: 'kelompok',
                render: function (data, type, row, meta) {
                    if (row.kelompok == 1){
                        return 'Supir';
                    }else if (row.kelompok == 2){
                        return 'Kenek';
                    }else if (row.kelompok == 3){
                        return 'Tamu';
                    }else if (row.kelompok == 4){
                        return 'Karyawan';
                    }else{
                        return 'Tamu';
                    }
                }
            },
            { data: 'id',
                render: function (data, type, row) {
                    // console.table(type);
                    let doAddPersonal =
                        '<button type="button" class="btn btn-default btn-sm"  onclick=doAddPersonal(' +data + '); ><i class="fa fa-plus"></i></button>';
                    return doAddPersonal;
                }
            }
        ]
    });

    function doAddPersonal(idx) {
        // console.log(idx);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route ('supir_edit') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                // console.log(datax);
                $('.supirId').val(datax.id);
                $('.kodeSupir').val(datax.kode_personal);
                $('.supirDescr').val(datax.nama);
                $('.close').click();
            }
        });
    }

    function hideAwalTab() {
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
        $('#panel-ubah').hide();
    }

    function tampilTambahTab() {
        $('#panel-tambah').show();
        $('#panel-tambah').click();
        $('#panel-data').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
    }

    function tampilDetailTab() {
        $('#panel-detail').show();
        $('#panel-detail').click();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
        $('#panel-tambah').hide();
    }

    function tampilDataTab() {
        $('#panel-data').show();
        $('#panel-data').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
    }

    function tampilUpdateTab() {
        $('#panel-ubah').show();
        $('#panel-ubah').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
    }

    $('#formInput').on('submit', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route ('kendaraan_input') }}",
            data: $(this).serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS ADD DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listKendaraan.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        }); 
    });

    function buttonEdit(idx) {
        $('.tab-content').focus();
        console.log(idx);
        tampilUpdateTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('kendaraan_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#editId').val(data.id);
                $('#editCode').val(data.kode_gate);
                $('#editKelompok').val(data.kelompok);
                $('#editNama').val(data.nama);
                $('#editRoom').val(data.ante_room);
                $('#editPrinter').val(data.printer);
                $('#editStatus').val(data.status);
            }
        });
    }

    $('#formUpdate').on("submit", function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{route('kendaraan_update')}}",
            data: $('#formUpdate').serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS UPDATE DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listKendaraan.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        });
    });

    function buttonDetail(idx) {
        console.log(idx);
        tampilDetailTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('kendaraan_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                
            }
        });
    }

    function buttonHapus (idx) {
        swal.fire({
            title: 'Apa Kamu Yakin ?',
            text: "Anda tidak akan dapat mengembalikan ini!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus ini!',
            cancelButtonText: 'Tidak, batal!',
            reverseButtons: true
            }).then(function (result) {
            if (result.value) {
                swal.fire(
                    'Hapus!',
                    'File Anda telah dihapus.',
                    'success'
                )
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $("input[name='_token']").val()
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{route('kendaraan_delete')}}",
                    data: {id: idx},
                    success: function (data) {
                    listKendaraan.ajax.reload();
                    }
                });
            }
        });
    }

</script>
@endsection
