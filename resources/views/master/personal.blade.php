@extends('template.layout')


@section ('css')
<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

<!--end::Global Theme Styles -->
<link href="{{ asset('assets/plugins/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<!--begin::Layout Skins(used by all pages) -->
<link href="{{ asset('/assets/vendors/datatables/datatables.bundle.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/mk-notifications.css') }}" rel="stylesheet"  type="text/css" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/themes/mk-theme-more.css') }}" rel="stylesheet"  type="text/css" />
<style type="text/css">
    .notactive {
        pointer-events: none;
        cursor: default;
    }
</style>
@endsection

@section ('content-header')
<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">
        MASTER </h3>
    <span class="kt-subheader__separator kt-hidden"></span>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="#" class="kt-subheader__breadcrumbs-link">Personal / Supir</a>
        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
    </div>
</div>
<div class="kt-subheader__toolbar">

</div>
@endsection


@section ('content-body')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Personal / Supir
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-right" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_1_tab_content"
                            role="tab" id="panel-data">
                            <i class="flaticon-lifebuoy"></i> DATA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_tab_content" role="tab"
                            id="panel-tambah">
                            <i class="flaticon-lifebuoy"></i> INSERT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_tab_content" role="tab"
                            id="panel-ubah">
                            <i class="flaticon-lifebuoy"></i> EDIT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_4_tab_content" role="tab"
                            id="panel-detail">
                            <i class="flaticon-lifebuoy"></i> DETAIL
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="kt_portlet_base_demo_1_tab_content" role="tabpanel">
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_1" id="tambah">INSERT</button>
                                        {{-- <button type="button" class="btn btn-outline-success float-right btn-sm" data-toggle="kt-tooltip" data-placement="bottom" title="Import File"><i class="fa fa-file-import"></i></button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="kt-portlet__body">
                        <!--begin: Datatable -->
                        <table class="table table-sm table-striped- table-bordered table-hover table-checkable" id="listPersonal">
                            <thead>
                                <tr>
                                    <th width=5%>NO</th>
                                    <th>KODE PERSONAL</th>
                                    <th>NAMA</th>
                                    <th>KELOMPOK</th>
                                    <th width=25%>ACTION</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable -->
                    </div>
                </div>

                <!-- Input data -->
                <div class="tab-pane" id="kt_portlet_base_demo_2_tab_content" role="tabpanel">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" action="#" id="formInput">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="id" name="id" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Personal<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="code" id="code" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">No. KTP / SIM<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="no_id" id="no_id" placeholder="No. ID" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Nama<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Name" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Alamat<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="alamat" id="alamat" placeholder="Address" oninput="this.value = this.value.toUpperCase()" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kota<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="kota" id="kota" placeholder="City" oninput="this.value = this.value.toUpperCase()" >
                                        </div>
                                        <label class="col-lg-2 col-form-label">No.HP<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="number" class="form-control form-control-sm" name="hp" id="hp" placeholder="No.HP" oninput="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kelompok<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <select name="kelompok" id="kelompok" class="form-control form-control-sm">
                                                <option value="">--PILIH--</option>
                                                <option value="1">Supir</option>
                                                <option value="2">Kenek</option>
                                                <option value="3">Tamu</option>
                                                <option value="4">Karyawan</option>
                                                <option value="5">Other</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Kode Perusahaan<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="perusahaanId" id="perusahaanId" class="form-control form-control-sm idperusahaan">
                                                <input type="text" id="perusahaanDescr" class="form-control form-control-sm perusahaanDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariPerusahaan" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalPerusahaan"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">INSERT</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
                <!-- input data -->

                <!-- Ubah Data -->
                <div class="tab-pane" id="kt_portlet_base_demo_3_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formUpdate">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="editId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">No Pintu<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editCode" id="editCode" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Kelompok<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <select name="editKelompok" id="editKelompok" class="form-control form-control-sm" required>
                                                <option value="">--PILIH--</option>
                                                <option value="K">K</option>
                                                <option value="B">B</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Nama Pintu<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editNama" id="editNama" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Ante Room<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editRoom" id="editRoom" placeholder="Description" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Printer Label Putaway<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editPrinter" id="editPrinter" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Status [A]ktif<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <select name="editStatus" id="editStatus" class="form-control form-control-sm" required>
                                                <option value="">--PILIH--</option>
                                                <option value="1">AKTIF</option>
                                                <option value="0">TIDAK AKTIF</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">UPDATE</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Ubah Data -->

                <!-- Detail -->
                <div class="tab-pane" id="kt_portlet_base_demo_4_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formDetail">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="detailId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">CODE<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editCode" id="detailCode" placeholder="Code" oninput="this.value = this.value.toUpperCase()" readonly>
                                        </div>
                                        <label class="col-lg-2 col-form-label">DESCRIPTION<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="editDescr" id="detailDescr" placeholder="Description" oninput="this.value = this.value.toUpperCase()" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="button" class="btn btn-secondary kembali">CLOSE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Detailll -->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal Perusahaan -->
<div class="modal fade" id="modalPerusahaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LIST Perusahaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="listSupplier">
                        <thead>
                            <tr>
                                <th width=5%>NO</th>
                                <th>KODE PERUSAHAAN</th>
                                <th>NAMA PERUSAHAAN</th>
                                <th>KELOMPOK</th>
                                <th width=20%>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Perusahaan-->
@endsection



@section('js')
<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/mknotif/js/mk-notifications.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
@endsection

@section ('jscustom')
<script>

    var listPersonal;

    $(document).ready(function () {    
        mkNotifications({
            positionX: "right",
            positionY: "top"
        });
        
        $('#tambah').click(function () {
            tampilTambahTab();
        });

        $('.kembali').click(function () {
            tampilDataTab();
        });
        hideAwalTab();

        listPersonal = $('#listPersonal').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('supir_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'kode_personal', name: 'kode_personal' },
                { data: 'nama', name: 'nama' },
                { data: 'kelompok',
                    render: function (data, type, row, meta) {
                        if (row.kelompok == 1){
                            return 'Supir';
                        }else if (row.kelompok == 2){
                            return 'Kenek';
                        }else if (row.kelompok == 3){
                            return 'Tamu';
                        }else if (row.kelompok == 4){
                            return 'Karyawan';
                        }else{
                            return 'Tamu';
                        }
                    }
                },
                { data: 'id',
                    render: function (data, type, row) {
                        // console.log(type);
                        let url = '{{ url("/master/personal/print/id") }}';
                            url = url.replace('id', row.id);
                        let buttonDetail = '<button type="button" class="btn btn-default btn-sm "  id="lihat" onclick=buttonDetail(' + data + '); '+row.r+'>Detail</button>'
                        let buttonEdit = '<button type="button" class="btn btn-default btn-sm ubah" id="ubah" data-toggle="modal" data-target="#kt_modal_edit"  onclick="buttonEdit(\''+data+'\');" '+row.u+'>Update</button>'
                        let buttonDelete = '<button type="button" class="btn btn-default btn-sm hapus" onclick="buttonHapus(\''+data+'\');" '+row.d+'>Delete</button>'
                        let buttonPrint = '<a href="'+url+'" class="btn btn-default btn-sm">Print</a>';
                        return buttonPrint+buttonDetail+buttonEdit+buttonDelete;
                    }
                }
            ]
        });

        listSupplier = $('#listSupplier').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('supplier_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'kode_sup', name: 'kode_sup' },
                { data: 'nama', name: 'nama' },
                { data: 'kelompok', name: 'kelompok' },
                { data: 'id',
                    render: function (data, type, row) {
                        // console.table(type);
                        let doAdd =
                            '<button type="button" class="btn btn-default btn-sm"  onclick=doAdd(' +
                            data + '); ><i class="fa fa-plus"></i></button>';
                        return doAdd;
                    }
                }
            ]
        });
    });
    

    function hideAwalTab() {
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
        $('#panel-ubah').hide();
    }

    function tampilTambahTab() {
        $('#panel-tambah').show();
        $('#panel-tambah').click();
        $('#panel-data').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
    }

    function tampilDetailTab() {
        $('#panel-detail').show();
        $('#panel-detail').click();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
        $('#panel-tambah').hide();
    }

    function tampilDataTab() {
        $('#panel-data').show();
        $('#panel-data').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
    }

    function tampilUpdateTab() {
        $('#panel-ubah').show();
        $('#panel-ubah').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
    }

    function doAdd(idx) {
        // console.log(idx);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route ('supplier_edit') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                // console.log(datax);
                $('.idperusahaan').val(datax.id);
                $('.perusahaanDescr').val(datax.nama);
                $('.close').click();
            }
        });
    }

    $('#formInput').on('submit', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route ('supir_input') }}",
            data: $(this).serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS ADD DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listPersonal.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        }); 
    });

    function buttonEdit(idx) {
        $('.tab-content').focus();
        console.log(idx);
        tampilUpdateTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('supir_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#editId').val(data.id);
                $('#editCode').val(data.kode_gate);
                $('#editKelompok').val(data.kelompok);
                $('#editNama').val(data.nama);
                $('#editRoom').val(data.ante_room);
                $('#editPrinter').val(data.printer);
                $('#editStatus').val(data.status);
            }
        });
    }

    $('#formUpdate').on("submit", function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{route('supir_update')}}",
            data: $('#formUpdate').serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS UPDATE DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listPersonal.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        });
    });

    function buttonDetail(idx) {
        console.log(idx);
        tampilDetailTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('supir_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                
            }
        });
    }

    function buttonHapus (idx) {
        swal.fire({
            title: 'Apa Kamu Yakin ?',
            text: "Anda tidak akan dapat mengembalikan ini!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus ini!',
            cancelButtonText: 'Tidak, batal!',
            reverseButtons: true
            }).then(function (result) {
            if (result.value) {
                swal.fire(
                    'Hapus!',
                    'File Anda telah dihapus.',
                    'success'
                )
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $("input[name='_token']").val()
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{route('tipe_delete')}}",
                    data: {id: idx},
                    success: function (data) {
                    listPersonal.ajax.reload();
                    }
                });
            }
        });
    }

</script>
@endsection
