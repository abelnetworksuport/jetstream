@extends('template.layout')
@section ('css')

@endsection

@section ('content-header')
<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">
        Dashboard </h3>
    <span class="kt-subheader__separator kt-hidden"></span>
</div>
<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <div class="btn kt-subheader__btn-daterange">

            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
            <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
        </div>
    </div>
</div>
@endsection
@section ('content-body')
<div class="row">
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin:: Widgets/Personal Income-->
                <div
                    class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__space-x">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title kt-font-light">
                                {{-- PT. EKA CIPTA MANDIRI --}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget27">
                            <div class="kt-widget27__visual">
                                <img src="./assets/media//bg/bg-4.jpg" alt="">
                                <h3 class="kt-widget27__title">
                                    <!-- <span><span>$</span>256,100</span> -->
                                </h3>
                                {{-- <div class="kt-widget27__btn">
                                    <a href="#" class="btn btn-pill btn-light btn-elevate btn-bold">Inclusive All Earnings</a>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Personal Income-->
            </div>
        </div>
        <!--End::Section-->

        <div class="kt-portlet">
            <div class="kt-portlet__body  kt-portlet__body--fit">
                <div class="row row-no-padding row-col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::Total Profit-->
                        <div class="kt-widget24">
                            <div class="kt-widget24__details">
                                <div class="kt-widget24__info">
                                    <h4 class="kt-widget24__title">
                                        Today IN
                                    </h4>
                                    {{-- <span class="kt-widget24__desc">
                                        Today Unloading
                                    </span> --}}
                                    
                                </div>
                                <span id="count_ul" class="kt-widget24__stats kt-font-brand">
                                    12
                                </span>
                            </div>
                            <div class="progress progress--sm">
                                <div class="progress-bar kt-bg-brand" role="progressbar" style="width: 80%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            {{-- <div class="kt-widget24__action">
                                <span class="kt-widget24__change">
                                    Change
                                </span>
                                <span class="kt-widget24__number">
                                    
                                </span>
                            </div> --}}
                        </div>

                        <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Feedbacks-->
                        <div class="kt-widget24">
                            <div class="kt-widget24__details">
                                <div class="kt-widget24__info">
                                    <h4 class="kt-widget24__title">
                                        Today OUT
                                    </h4>
                                    {{-- <span class="kt-widget24__desc">
                                        Today Receive
                                    </span> --}}
                                </div>
                                <span id="count_rcs" class="kt-widget24__stats kt-font-warning">
                                    14
                                </span>
                            </div>
                            <div class="progress progress--sm">
                                <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 80%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            {{-- <div class="kt-widget24__action">
                                <span class="kt-widget24__change">
                                    Change
                                </span>
                                <span class="kt-widget24__number">
                                    84%
                                </span>
                            </div> --}}
                        </div>

                        <!--end::New Feedbacks-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Orders-->
                        {{-- <div class="kt-widget24">
                            <div class="kt-widget24__details">
                                <div class="kt-widget24__info">
                                    <h4 class="kt-widget24__title">
                                        Departure
                                    </h4>
                                    <span class="kt-widget24__desc">
                                        Today Departure
                                    </span>
                                </div>
                                <span id="count_dep" class="kt-widget24__stats kt-font-danger">
                                    
                                </span>
                            </div>
                            <div class="progress progress--sm">
                                <div class="progress-bar kt-bg-danger" role="progressbar" style="width: 80%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="kt-widget24__action">
                                <span class="kt-widget24__change">
                                    Change
                                </span>
                                <span class="kt-widget24__number">
                                    69%
                                </span>
                            </div>
                        </div> --}}

                        <!--end::New Orders-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">

                        <!--begin::New Users-->
                        {{-- <div class="kt-widget24">
                            <div class="kt-widget24__details">
                                <div class="kt-widget24__info">
                                    <h4 class="kt-widget24__title">
                                        Arrived
                                    </h4>
                                    <span class="kt-widget24__desc">
                                        Today Arrived
                                    </span>
                                </div>
                                <span id="count_arr" class="kt-widget24__stats kt-font-success">
                                    
                                </span>
                            </div>
                            <div class="progress progress--sm">
                                <div class="progress-bar kt-bg-success" role="progressbar" style="width: 80%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="kt-widget24__action">
                                <span class="kt-widget24__change">
                                    Change
                                </span>
                                <span class="kt-widget24__number">
                                    90%
                                </span>
                            </div>
                        </div> --}}

                        <!--end::New Users-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end:: Content -->
</div>
@endsection

@section('footer')
@endsection
@section ('js')
<script src="{{ asset('assets/js/pages/components/charts/flotcharts.js')}}" type="text/javascript"></script>
{{-- <script src="{{ asset('assets/plugins/general/dompurify/dist/purify.js') }}" type="text/javascript"></script> --}}
<!--begin::Page Vendors(used by this page) -->

<!-- ams chart -->
<script src="{{asset('assets/js/pages/components/charts/amcharts/amcharts.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/components/charts/amcharts/serial.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/components/charts/amcharts/light.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<script>

    $(document).ready(function(){

    });
</script>
@endsection
