@extends('template.layout')


@section ('css')
<!--begin::Global Theme Styles(used by all pages) -->
<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

<!--end::Global Theme Styles -->
<link href="{{ asset('assets/plugins/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<!--begin::Layout Skins(used by all pages) -->
<link href="{{ asset('/assets/vendors/datatables/datatables.bundle.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/mk-notifications.css') }}" rel="stylesheet"  type="text/css" />
<link href="{{ asset('/assets/plugins/general/mknotif/css/themes/mk-theme-more.css') }}" rel="stylesheet"  type="text/css" />
<style type="text/css">
    .notactive {
        pointer-events: none;
        cursor: default;
    }
</style>
@endsection

@section ('content-header')
<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">
        MASTER </h3>
    <span class="kt-subheader__separator kt-hidden"></span>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="#" class="kt-subheader__breadcrumbs-link">ITEMS</a>
        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
    </div>
</div>
<div class="kt-subheader__toolbar">

</div>
@endsection


@section ('content-body')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    ITEMS
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-right" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_1_tab_content"
                            role="tab" id="panel-data">
                            <i class="flaticon-lifebuoy"></i> DATA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_tab_content" role="tab"
                            id="panel-tambah">
                            <i class="flaticon-lifebuoy"></i> INSERT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_tab_content" role="tab"
                            id="panel-ubah">
                            <i class="flaticon-lifebuoy"></i> EDIT
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_4_tab_content" role="tab"
                            id="panel-detail">
                            <i class="flaticon-lifebuoy"></i> DETAIL
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="kt_portlet_base_demo_1_tab_content" role="tabpanel">
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_1" id="tambah">INSERT</button>
                                        {{-- <button type="button" class="btn btn-outline-success float-right btn-sm" data-toggle="kt-tooltip" data-placement="bottom" title="Import File"><i class="fa fa-file-import"></i></button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="kt-portlet__body">
                        <!--begin: Datatable -->
                        <table class="table table-sm table-striped- table-bordered table-hover table-checkable" id="listItem">
                            <thead>
                                <tr>
                                    <th width=5%>NO</th>
                                    <th>KODE ITEM</th>
                                    <th>DESCRIPTION</th>
                                    <th>JENIS</th>
                                    <th width=20%>ACTION</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable -->
                    </div>
                </div>

                <!-- Input data -->
                <div class="tab-pane" id="kt_portlet_base_demo_2_tab_content" role="tabpanel">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" action="#" id="formInput">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="id" name="id" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Rack<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="code" id="code" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Description</label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="description" id="description" placeholder="description" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Jenis Item<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="jenisId" id="jenisId" class="form-control form-control-sm jenisId">
                                                <input type="text" id="jenisDescr" class="form-control form-control-sm jenisDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariJenis" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalJenis"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">INSERT</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
                <!-- input data -->

                <!-- Ubah Data -->
                <div class="tab-pane" id="kt_portlet_base_demo_3_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formUpdate">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="editId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Rack<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="code" id="code" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Description</label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="description" id="description" placeholder="description" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Jenis Item<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="jenisId" id="jenisId" class="form-control form-control-sm jenisId">
                                                <input type="text" id="jenisDescr" class="form-control form-control-sm jenisDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariJenis" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalJenis"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary">UPDATE</button>
                                        <button type="button" class="btn btn-secondary kembali">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Ubah Data -->

                <!-- Detail -->
                <div class="tab-pane" id="kt_portlet_base_demo_4_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formDetail">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="editId" name="detailId" required>
                            <div class="kt-portlet__body">
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Kode Rack<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="code" id="code" placeholder="Code" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Description</label>
                                        <div class="col-lg-4 col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="description" id="description" placeholder="description" oninput="this.value = this.value.toUpperCase()" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-lg-2 col-form-label">Jenis Item<span class="required" style="color:red;">*</span></label>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="input-group">
                                                <input type="hidden" name="jenisId" id="jenisId" class="form-control form-control-sm jenisId">
                                                <input type="text" id="jenisDescr" class="form-control form-control-sm jenisDescr" placeholder="SEARCH"  readonly>
                                                <div class="input-group-append">
                                                    <button id="cariJenis" type="button" class="btn btn-default btn-custom btn-sm" data-toggle="modal" data-target="#modalJenis"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="float-right">
                                        <button type="button" class="btn btn-secondary kembali">CLOSE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!-- Detailll -->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal Jenis -->
<div class="modal fade" id="modalJenis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Jenis Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="listJenis">
                        <thead>
                            <tr>
                                <th width=5%>NO</th>
                                <th>Kode Jenis</th>
                                <th>Description</th>
                                <th width=20%>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal Jenis-->
@endsection



@section('js')
<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/general/mknotif/js/mk-notifications.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
@endsection

@section ('jscustom')
<script>

    var listItem;

    $(document).ready(function () {    
        mkNotifications({
            positionX: "right",
            positionY: "top"
        });
        
        $('#tambah').click(function () {
            tampilTambahTab();
        });

        $('.kembali').click(function () {
            tampilDataTab();
        });
        hideAwalTab();

        listItem = $('#listItem').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('item_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'kode_item', name: 'kode_gate' },
                { data: 'description', name: 'description' },
                { data: 'jenis_id', name: 'jenis_id' },
                { data: 'id',
                    render: function (data, type, row) {
                        // console.log(type);
                        let buttonDetail = '<button type="button" class="btn btn-default btn-sm "  id="lihat" onclick=buttonDetail(' + data + '); '+row.r+'>Detail</button>'
                        let buttonEdit = '<button type="button" class="btn btn-default btn-sm ubah" id="ubah" data-toggle="modal" data-target="#kt_modal_edit"  onclick="buttonEdit(\''+data+'\');" '+row.u+'>Update</button>'
                        let buttonDelete = '<button type="button" class="btn btn-default btn-sm hapus" onclick="buttonHapus(\''+data+'\');" '+row.d+'>Delete</button>'
                        return buttonDetail+buttonEdit+buttonDelete;
                    }
                }
            ]
        });

    });

    listJenis = $('#listJenis').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('jenis_list')}}",
        columns: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'kode_jenis', name: 'kode_jenis' },
            { data: 'description', name: 'description' },
            {
                data: 'id',
                render: function (data, type, row) {
                    // console.table(type);
                    let doAdd =
                        '<button type="button" class="btn btn-default btn-sm"  onclick=doAdd(' +
                        data + '); ><i class="fa fa-plus"></i></button>';
                    return doAdd;
                }
            }
        ]
    });

    function doAdd(idx) {
        // console.log(idx);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route ('jenis_edit') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                // console.log(datax);
                $('.jenisId').val(datax.id);
                $('.jenisDescr').val(datax.description);
                $('.close').click();
            }
        });
    }
    
    function hideAwalTab() {
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
        $('#panel-ubah').hide();
    }

    function tampilTambahTab() {
        $('#panel-tambah').show();
        $('#panel-tambah').click();
        $('#panel-data').hide();
        $('#panel-ubah').hide();
        $('#panel-detail').hide();
    }

    function tampilDetailTab() {
        $('#panel-detail').show();
        $('#panel-detail').click();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
        $('#panel-tambah').hide();
    }

    function tampilDataTab() {
        $('#panel-data').show();
        $('#panel-data').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
    }

    function tampilUpdateTab() {
        $('#panel-ubah').show();
        $('#panel-ubah').click();
        $('#panel-detail').hide();
        $('#panel-tambah').hide();
        $('#panel-ubah').hide();
        $('#panel-data').hide();
    }

    $('#formInput').on('submit', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route ('item_input') }}",
            data: $(this).serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS ADD DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listItem.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        }); 
    });

    function buttonEdit(idx) {
        $('.tab-content').focus();
        console.log(idx);
        tampilUpdateTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('item_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#editId').val(data.id);
                $('#editCode').val(data.kode_gate);
                $('#editKelompok').val(data.kelompok);
                $('#editNama').val(data.nama);
                $('#editRoom').val(data.ante_room);
                $('#editPrinter').val(data.printer);
                $('#editStatus').val(data.status);
            }
        });
    }

    $('#formUpdate').on("submit", function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{route('item_update')}}",
            data: $('#formUpdate').serialize(),
            success: function(response) {
                mkNoti(
                    '', 
                    'SUCCESS UPDATE DATA MASTER',
                    {
                        sound: true,
                        status: "success"
                    }
                );
                listItem.ajax.reload();
                $('.kembali').click();
                $('#formInput').trigger("reset");
            } 
        });
    });

    function buttonDetail(idx) {
        console.log(idx);
        tampilDetailTab();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('item_edit')}}",
            data: { id: idx },
            success: function (data) {
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                $('#detailCode').val(data.kode_uom);
                $('#detailDescr').val(data.descr);
                
            }
        });
    }

    function buttonHapus (idx) {
        swal.fire({
            title: 'Apa Kamu Yakin ?',
            text: "Anda tidak akan dapat mengembalikan ini!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus ini!',
            cancelButtonText: 'Tidak, batal!',
            reverseButtons: true
            }).then(function (result) {
            if (result.value) {
                swal.fire(
                    'Hapus!',
                    'File Anda telah dihapus.',
                    'success'
                )
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $("input[name='_token']").val()
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{route('item_delete')}}",
                    data: {id: idx},
                    success: function (data) {
                    listItem.ajax.reload();
                    }
                });
            }
        });
    }

</script>
@endsection
