<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('assets/css/production.css') }}" rel="stylesheet" type="text/css" />
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <script src="{{ asset('assets/js/jquery.qrcode.min.js') }}"></script>
    <title>Print Kendaraan</title>
</head>
<body>

    <!-- <div class="print">
    <div class="column">
        <div class="visible-print text-center">{!! QrCode::size(100)->generate(Request::url()); !!}</div>
        <p class="barcode">f</p>
        <p class="qty">f</p>  
        </div>
    </div> -->
    <div class="noprint">
        <div class="header-fixed">
            <div class="centerize">   
                <button type="button" class="btn btn-primary btn-icon" id="buttonPrint" onclick="myFunction()">PRINT</button>
                <button type="button" class="btn btn-danger btn-icon" id="buttonBack" onclick="backFunction()">BACK</button>
            </div>
        </div>
    </div>

    <input id="idData" type="hidden" value="{{$idx}}">
    <div id="tess">
        <!-- Append Here -->
    </div>

<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->
<script>

    $(document).ready(function(){
        generate_qrcode();
    });

    function generate_qrcode() {
        var idData = $('#idData').val();
        console.log(idData);
        $.ajax({
            type: "GET",
            url: "{{route('kendaraan_qr')}}",
            data :{
                id : idData
            },
            success: function (datax) {
                console.log(datax);
                $.each(datax, function (index, value) {
                    /* $('#supplierInput').append('<option id=' + value.id + ' value=' + value
                        .id + '>' + value.description + '</option>') */
                        $('#tess').append(
                            '<div class="print">'+
                                '<div class="column">'+
                                        '<div class="qrcode" id="qrcode'+ value.id + '" style="float: right; margin-top: 5px;"></div>'+
                                        '<div class="barcode">'+value.no_polisi+'</div>'+
                                        '<div class="code" id="code">'+value.tahun+'</span></div>'+
                                        // '<div class="bundle">'+value.bundle+' </div>'+
                                        // '<div class="material">' +value.material+' - '+value.grade+'</div>'+
                                '</div>'+
                            '</div>'
                        );
                        $('#qrcode canvas').remove();
                        $('#qrcode'+ value.id + '').qrcode({
                            render: 'canvas',
                            text: value.no_polisi,
                            width: 90,
                            height: 90
                        });
                });
            }
        });
        //console.log(idData);    
    }

    function myFunction() {
		window.print();
	}

    function backFunction() {
        window.history.back();
    }

</script>

</body>
</html>
